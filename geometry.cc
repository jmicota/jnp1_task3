#include "geometry.h"

bool Rectangles::operator==(const Rectangles &rhs) const {
    if (size() != rhs.size())
        return false;
    auto count = size();
    for (size_t i = 0; i < count; i++) {
        if ((*this)[i] != rhs[i])
            return false;
    }
    return true;
}

Rectangles & Rectangles::operator+=(const Vector &v) {
    for (auto &rect : _vector)
        rect += v;
    return *this;
}

Rectangle merge_horizontally(Rectangle const &l, Rectangle const &r) {
    assert(l.pos().x() == r.pos().x() && l.width() == r.width());
    assert(l.pos().y() + l.height() == r.pos().y() || r.pos().y() + r.height() == l.pos().y());
    auto ny = std::min(l.pos().y(), r.pos().y());
    return Rectangle(l.width(), l.height() + r.height(), Position(l.pos().x(), ny));
}

Rectangle merge_vertically(Rectangle const &l, Rectangle const &r) {
    assert(l.pos().y() == r.pos().y() && l.height() == r.height());
    assert(l.pos().x() + l.width() == r.pos().x() || r.pos().x() + r.width() == l.pos().x());
    auto nx = std::min(l.pos().x(), r.pos().x());
    return Rectangle(l.width() + r.width(), l.height(), Position(nx, l.pos().y()));
}

Rectangle merge(Rectangle const &l, Rectangle const &r) {
    if (l.pos().x() == r.pos().x())
        return merge_horizontally(l, r);
    else
        return merge_vertically(l, r);
}

Rectangle merge_all(Rectangles const &r) {
    assert(r.size() > 0);
    auto ret = r[0];
    auto count = r.size();
    for (size_t i = 1; i < count; i++)
        ret = merge(ret, r[i]);
    return ret;
}