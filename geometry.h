#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <cstdint>
#include <cassert>
#include <algorithm>
#include <vector>

using DataType = int32_t;

template <typename StrictType>
class VectorBase {

private:
    DataType _x, _y;

public:
    VectorBase(DataType x, DataType y) : _x(x), _y(y) {}
    template <typename T>
    VectorBase(VectorBase<T> const &v) : _x(v.x()), _y(v.y()) {}

    DataType x() const {
        return _x;
    }

    DataType y() const {
        return _y;
    }

    bool operator==(StrictType const &rhs) const {
        return x() == rhs.x() && y() == rhs.y();
    }

    bool operator!=(StrictType const &rhs) const {
        return x() != rhs.x() || y() != rhs.y();
    }

};

class Vector : public VectorBase<Vector> {

private:
    using Base = VectorBase<Vector>;

public:
    Vector(DataType x, DataType y) : Base(x, y) {}
    Vector(Vector const &v) : Base(v) {}
    template <typename T>
    explicit Vector(VectorBase<T> const &v) : Base(v) {}

    Vector reflection() const {
        return Vector(this->y(), this->x());
    }

    Vector &operator+=(Vector const &rhs) {
        *this = Vector(this->x() + rhs.x(), this->y() + rhs.y());
        return *this;
    }

};

class Position : public VectorBase<Position> {

private:
    using Base = VectorBase<Position>;

public:
    Position(DataType x, DataType y) : Base(x, y) {}
    Position(Position const &v) : Base(v) {}
    template <typename T>
    explicit Position(VectorBase<T> const &v) : Base(v) {}

    Position reflection() const {
        return Position(this->y(), this->x());
    }

    static Position const &origin() {
        static Position value (0, 0);
        return value;
    }

    Position &operator+=(Vector const &rhs) {
        *this = Position(this->x() + rhs.x(), this->y() + rhs.y());
        return *this;
    }

};

inline Vector operator+(Vector const &l, Vector const &r) {
    return Vector(l.x() + r.x(), l.y() + r.y());
}

inline Position operator+(Position const &p, Vector const &v) {
    return Position(p.x() + v.x(), p.y() + v.y());
}

inline Position operator+(Vector const &v, Position const &p) {
    return p + v;
}


struct Rectangle {

private:
    DataType _w, _h;
    Position _pos;

public:
    Rectangle(DataType w, DataType h, Position const &pos) : _w(w), _h(h), _pos(pos) {
        assert(w > 0 && h > 0);
    }

    Rectangle(DataType w, DataType h) : Rectangle(w, h, Position(0, 0)) {}

    DataType width() const {
        return _w;
    }

    DataType height() const {
        return _h;
    }

    Position pos() const {
        return _pos;
    }

    DataType area() const {
        auto ret = width() * height();
        assert(width() == 0 || ret / width() == height()); // check for overflow
        return ret;
    }

    Rectangle reflection() const {
        return Rectangle(height(), width(), pos().reflection());
    }

    bool operator==(Rectangle const &rhs) const {
        return width() == rhs.width() && height() == rhs.height() && pos() == rhs.pos();
    }

    bool operator!=(Rectangle const &rhs) const {
        return !(*this == rhs);
    }

    Rectangle &operator+=(Vector const &rhs) {
        _pos += rhs;
        return *this;
    }

};

inline Rectangle operator+(Rectangle const &r, Vector const &v) {
    return Rectangle(r.width(), r.height(), r.pos() + v);
}

inline Rectangle operator+(Vector const &v, Rectangle const &r) {
    return r + v;
}

Rectangle merge_horizontally(Rectangle const &l, Rectangle const &r);

Rectangle merge_vertically(Rectangle const &l, Rectangle const &r);

Rectangle merge(Rectangle const &l, Rectangle const &r);


class Rectangles {

private:
    std::vector<Rectangle> _vector;

public:
    Rectangles() {}
    Rectangles(std::initializer_list<Rectangle> list) : _vector(list) {}
    Rectangles(Rectangles const &r) : _vector(r._vector) {}
    Rectangles(Rectangles &&r) noexcept : _vector(std::move(r._vector)) {}

    size_t size() const {
        return _vector.size();
    }

    Rectangle const &operator[](size_t i) const {
        return _vector[i];
    }

    Rectangle &operator[](size_t i) {
        return _vector[i];
    }

    Rectangles &operator=(Rectangles const &rhs) {
        _vector = rhs._vector;
        return *this;
    }

    Rectangles &operator=(Rectangles &&rhs) noexcept {
        _vector = std::move(rhs._vector);
        return *this;
    }

    bool operator==(Rectangles const &rhs) const;

    bool operator!=(Rectangles const &rhs) const {
        return !(*this == rhs);
    }

    Rectangles &operator+=(Vector const &v);

};

inline Rectangles operator+(Rectangles const &r, Vector const &v) {
    Rectangles copy (r);
    copy += v;
    return copy;
}

inline Rectangles operator+(Vector const &v, Rectangles const &r) {
    return r + v;
}

Rectangle merge_all(Rectangles const &r);

#endif